<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
                $table->increments('id');
                $table->char('files', 45);
                $table->string('name_society', 45);
                $table->string('name_contact', 250);
                $table->string('email_contact', 45);
                $table->string('phone', 45);
                $table->string('activity', 250);
                $table->text('note');           
                $table->timestamps();
                $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
