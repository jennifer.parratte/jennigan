<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 45);
            $table->string('type', 45);
            $table->string('description', 250);
            $table->string('location', 250);
            $table->string('category', 250);
            $table->string('salary', 45);          
            $table->boolean('enable');
            $table->text('note');
            $table->dateTime('closing_at');
            $table->timestamps();
            $table->integer('client_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
